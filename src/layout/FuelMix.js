
import React, { Component } from 'react';
import * as d3 from "d3";
import '../css/Graphs.css';

const colors = d3.scaleOrdinal(d3.schemeCategory10);

class FuelMix extends Component {
/*
    Original implementation source:
    MIT License Copyright (c) 2017 Artyom Trityak
    link: https://github.com/artyomtrityak/d3-explorer
*/
    constructor(props) {
    super(props);
    this.state = {
        // data from ISO New England offical website 
      data: [
          {
                FuelCategory: "NATURAL GAS",
                GenMw: "50"
          },
          {
                FuelCategory: "HYDRO",
                GenMw: "12"
          },
          {
                FuelCategory: "NUCLEAR",
                GenMw: "30"
          },
          {
                FuelCategory: "RENEWABLES",
                GenMw: "8"
          }
      ]
    };
  }

  componentDidMount() {
    const width = window.innerWidth - 350,
      height = window.innerHeight - 200,
      outerRadius = height / 2 - 20,
      innerRadius = outerRadius / 2;

    const radius = Math.min(width, height) / 2,
          labelRadius = radius - 10;

    const chart = d3.select(this.chartRef)
      .attr("width", window.innerWidth - 100)
      .attr("height", height + 100)
      .append("g")
        .attr("transform", `translate(${width / 2}, ${height / 2})`);

    const pie = d3.pie()
      .sort(null)
      .value((d) => +d.GenMw)
      .padAngle(.02);

    const arc = d3.arc()
      .outerRadius(outerRadius - 20)
      .innerRadius(innerRadius);

    const arcsWithData = pie(this.state.data);

    const gContainer = chart.selectAll("path")
      .data(arcsWithData)
      .enter()
        .append('g');

    gContainer.append("path")
        .attr("fill", (d, i) => colors(i))
        .each((d) => {
          d.outerRadius = outerRadius - 20;
        })
        .attr("d", arc)
        .on("mouseover", this.arcTween(arc, outerRadius, true))
        .on("mouseout", this.arcTween(arc, outerRadius - 20));

    //Center label
    chart.append('text')
        .attr("text-anchor", "middle")
        .attr('dy', '0.35em')
        .style('font-size', '40px')
        .text('FUEL USAGE');

    //Inner labels
    gContainer.append("text")
        .attr("transform", (d) => {
          return `translate(${arc.centroid(d)})`;
        })
        .attr("dy", "0.35em")
        .style('fill', 'white')
        .text((d) => {
          return `${d.data.GenMw} %`;
        });

    //Outter labels
    gContainer.append('text')
      .attr("transform", (d) => {
        const c = arc.centroid(d);
        const x = c[0];
        const y = c[1];
        const h = Math.sqrt(x*x + y*y);
        return `translate(${x/h * labelRadius},${y/h * labelRadius})`;
      })
      .attr("text-anchor", (d) => {
        return (d.endAngle + d.startAngle) / 2 > Math.PI ? "end" : "start";
      })
      .attr("dy", ".35em")
      .style('font-size', '20px')
      .text((d) => {
        return `${d.data.FuelCategory}`;
      });
  }

  // add animation
  arcTween(arc, newOuterRadius, isMouseOver) {
    return function interpolateFn(data, i) {
      d3.select(this)
        .transition()
        .duration(1000)
        .attrTween("d", (d) => {
          const interpolator = d3.interpolate(d.outerRadius, newOuterRadius);
          return (t) => {
            d.outerRadius = interpolator(t);
            return arc(d);
          };
        })
        .attrTween('fill', (d) => {
          const to = isMouseOver ? 'white' : colors(i);
          return d3.interpolateRgb(this.getAttribute("fill"), to);
        });
    };
  }

    render() {
      return (
        <section id="FuelMix" className="fuelmix-section hideAnimation">
            <div className="container">
                <center className="row">
                    <div className="card">
                        <div className="card-block">
                            <h2 className="card-title"><i className="fa fa-pie-chart fa-lg" aria-hidden="true"></i> TODAY FULEMIX</h2>
                            <p className="card-text">This graph indicates the fuels being used by New Englands power plants to generate electricity.
                                <a href="https://www.iso-ne.com/isoexpress/web/charts/guest-hub?p_p_id=fiveminuterealtimelmp_WAR_isoneportlet_INSTANCE_a3zQ&p_p_lifecycle=0&p_p_state=normal&p_p_state_rcv=1"><i className="fa fa-lg fa-arrow-circle-right" aria-hidden="true"> Learn more</i></a>
                            </p>
                        </div>
                        <svg className="card-img-top img-fluid img-responsive" ref={(r) => this.chartRef = r}></svg>
                    </div>
                </center>
            </div>
            <a className="page-scroll" href="#VermontLMP">
                <i className="fa fa-4x fa-chevron-circle-down" aria-hidden="true"></i>
            </a>
        </section>
      );
    }
}

export default FuelMix;
