import React, { Component } from 'react';
import '../css/About.css';


class About extends Component {
  render() {
    return (
        <section id="About" className="about-section hideAnimation">
            <div className="container">
                <div className="row">
                    <center>
                      <h1 className="main-title">Why choose Packetized Energy?</h1>
                      <p>Advantages of our patent-pending system, Packetized Energy Management (PEM), include:</p>
                      <div className="card">
                          <div className="card-block">
                              <h3 className="card-title subtitle1">LOCAL DECISION-MAKING</h3>
                              <p className="card-text">Home or business manages its own devices based on unique needs</p>
                          </div>
                      </div>
                      <br />
                      <div className="card">
                          <div className="card-block">
                              <h3 className="card-title subtitle2">PRIVACY</h3>
                              <p className="card-text">Personal energy usage information is never shared with anyone, ever</p>
                          </div>
                      </div>
                      <br />
                      <div className="card">
                          <div className="card-block">
                              <h3 className="card-title subtitle3">FAIRNESS</h3>
                              <p className="card-text">All users have equitable access to the grid’s resources</p>
                          </div>
                      </div>
                      <br />
                      <div className="card">
                          <div className="card-block">
                              <h3 className="card-title subtitle4">SCALABILITY</h3>
                              <p className="card-text">Software systems that scale up to manage millions of devices</p>
                          </div>
                      </div>
                      <br />
                      <div className="card">
                          <div className="card-block">
                              <h3 className="card-title subtitle5">ADAPTIVE</h3>
                              <p className="card-text">Loads adapt to rapid changes in supply and demand with minimal coordination</p>
                          </div>
                      </div>
                      <hr />
                      <div className="footer">
                           Copyright &copy; 2017 Packetized Energy Technologies, Inc. All rights reserved.
                      </div>
                    </center>
                </div>
            </div>
            <a className="page-scroll" href="#page-top">
                <i className="fa fa-4x fa-chevron-circle-up" aria-hidden="true"></i>
            </a>
        </section>
    );
  }
}

export default About;
