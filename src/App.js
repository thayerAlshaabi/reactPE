
import React, { Component } from 'react';

// sub-pages
import Navigation from './layout//Navigation';
import Home from './layout//Home';
import FuelMix from './layout/FuelMix';
import VermontLMP from './layout/VermontLMP';
import About from './layout/About';

// stylesheets
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import './css/App.css';


class App extends Component {
    render() {
      return (
        <center className="App">
            <Navigation />
            <Home />
            <FuelMix />
            <VermontLMP />
            <About />
        </center>
      );
    }
}

export default App;
