
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});

$(function() {
    $(document).on('click', 'a.page-scroll', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');

        //get displaying tab content jQuery selector
        var active_tab = $('.navbar-right > li.activeAnimation > a').attr('href');

        //find actived navigation and remove 'active' css
        var actived_nav = $('.navbar-right > li.activeAnimation');
        actived_nav.removeClass('activeAnimation');

        //add 'active' css into clicked navigation
        $(this).parents('li').addClass('activeAnimation');

        //hide displaying tab content
        $(active_tab).removeClass('activeAnimation');
        $(active_tab).addClass('hideAnimation');

        //show target tab content
        var active_section = $(this).attr('href');
        $(active_section).removeClass('hideAnimation');
        $(active_section).addClass('activeAnimation');

        event.preventDefault();
    });
});
